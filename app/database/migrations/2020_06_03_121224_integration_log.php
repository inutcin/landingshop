<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IntegrationLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('integration_log', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->timestamps();

            $table->char('url', 255)
				->comment('url запроса')
            ;

            $table->tinyInteger('type_id')
                ->unsigned()
                ->comment('типзапроса (СС, в магазини пр.)')
            ;

            $table->longText('request')
				->comment('текст запроса')
            ;

            $table->longText('response')
				->comment('текст ответа')
            ;

            $table->integer('wait_time')
                ->unsigned()
				->comment('Время ответа в мс')
            ;

            $table->index('type_id');
            $table->foreign("type_id")->references("id")->on("log_type");
            
        });        

                

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
