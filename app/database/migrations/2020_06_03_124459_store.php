<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Store extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::create('store', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->char('title')
                ->unique()
				->comment('Название склада')
            ;

            $table->char('uid', 80)
                ->unique()
				->comment('uid 1C')
            ;

            $table->tinyInteger('active')
                ->unsigned()
                ->comment('Активность')
            ;
            $table->index('active');

            $table->index('uid');

        });        



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
