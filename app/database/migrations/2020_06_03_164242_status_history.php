<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StatusHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('status_history', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->timestamps();

            $table->bigInteger('order_id')
                ->unsigned()
				->comment('id заказав в лендинге')
            ;

            $table->tinyInteger('status_id')
                ->unsigned()
				->comment('статус заказа')
            ;

            $table->index('order_id');
            $table->index('status_id');

            $table->foreign("status_id")->references("id")->on("status");
            $table->foreign("order_id")->references("id")->on("landing_order");

        });        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
