<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OfferFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('offer_file', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->integer('offer_id')
                ->unsigned()
				->comment('Торговое предложение чей файл')
            ;

            $table->integer('file_id')
                ->unsigned()
				->comment('id файла')
            ;

            $table->index('offer_id');
            $table->index('file_id');
            $table->foreign("offer_id")->references("id")->on("offer");
            $table->foreign("file_id")->references("id")->on("attached_file");
        });        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
