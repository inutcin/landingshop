<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LogType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('log_type', function (Blueprint $table) {

            $table->tinyIncrements('id');
            $table->timestamps();

            $table->char('name', 32)
                ->unique()
				->comment('Название типа лога')
            ;
        });        

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
