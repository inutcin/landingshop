<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LandingOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('landing_order', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->timestamps();

            $table->bigInteger('shop_id')
                ->unsigned()
				->comment('id заказав магазине')
            ;

            $table->char('shop_num', 20)
				->comment('номер заказав магазине')
            ;

            $table->tinyInteger('status_id')
                ->unsigned()
				->comment('статус заказа')
            ;

            $table->bigInteger('user_id')
                ->unsigned()
				->comment('Пользователь чей заказ')
            ;

            $table->date('last_status_change')
                ->unsigned()
				->comment('Дата последнего изменения статуса')
            ;

            $table->index('shop_id');
            $table->index('status_id');
            $table->index('user_id');

            $table->foreign("status_id")->references("id")->on("status");
            $table->foreign("user_id")->references("id")->on("user");

        });        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
