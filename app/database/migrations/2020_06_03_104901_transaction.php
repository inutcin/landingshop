<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Transaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->timestamps();

            $table->bigInteger('user_id')
                ->unsigned()
                ->comment('Пользователь')
            ;

            $table->float('points', 12, 2)
				->comment('Количество баллов в транзакции')
            ;

            $table->index('user_id');
            $table->foreign("user_id")->references("id")->on("user");
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
