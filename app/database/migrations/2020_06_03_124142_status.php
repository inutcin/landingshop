<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Status extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('status', function (Blueprint $table) {

            $table->tinyIncrements('id');
            $table->timestamps();

            $table->char('code', 3)
                ->unique()
				->comment('Код статуса')
            ;

            $table->char('name', 32)
                ->unique()
				->comment('Название статуса')
            ;

            $table->index('code');

        });        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
