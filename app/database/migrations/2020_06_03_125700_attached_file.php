<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AttachedFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('attached_file', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->char('url', 255)
				->comment('Ссылка на файл без протокола и домена')
            ;

            $table->char('content_type', 64)
				->comment('Content-type файла')
            ;

            $table->integer('length')
				->comment('Размер файла в байтах')
            ;

            $table->integer('external_id')
				->comment('Внешний id файлов')
            ;
            
            $table->index('external_id');
        });        



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
