<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Voucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->timestamps();

            $table->float('points',12,2)
				->comment('Номинал ваучера')
            ;

            $table->bigInteger('user_id')
                ->unsigned()
                ->comment('id пользователя, который получил или обналичил ваучер')
            ;

            $table->char('value', 32)
                ->unique()
				->comment('Код ваучера')
            ;

            $table->bigInteger('transaction_id')
                ->unique()
                ->unsigned()
				->comment('id транзакции')
            ;

            $table->date('pass_date')
				->comment('Дата передачи ваучера пользователю')
            ;
            $table->date('expire_date')
				->comment('Дата до которого надо реализовать ваучер')
            ;
            $table->date('apply_date')
				->comment('Дата обналичивания ваучера')
            ;
            
            $table->index('transaction_id');
            $table->index('user_id');
            $table->foreign("transaction_id")->references("id")->on("transaction");
            $table->foreign("user_id")->references("id")->on("user");
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
