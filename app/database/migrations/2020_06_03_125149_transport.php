<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Transport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('transport', function (Blueprint $table) {

            $table->tinyIncrements('id');
            $table->timestamps();

            $table->char('code', 40)
                ->unique()
				->comment('Код транспорта')
            ;

            $table->char('name', 40)
                ->unique()
				->comment('Название транспорта')
            ;

            $table->index('code');

        });        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
