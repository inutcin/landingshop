<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('product_detail', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->integer('product_id')
                ->unsigned()
                ->comment('id товара для которого описание')
            ;

            $table->longText('description')
				->comment('Полное описание товара')
            ;

            $table->text('receive_rules')
				->comment('Правила получения')
            ;

            $table->text('cancel_rules')
				->comment('Правила отмены')
            ;

            $table->index('product_id');

            $table->foreign("product_id")->references("id")->on("product");
        });        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
