<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Section extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('section', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->char('name', 255)
                ->unique()
                ->comment('Название раздела')
            ;

            $table->char('code', 255)
                ->unique()
				->comment('Код раздела')
            ;

            $table->tinyInteger('active')
                ->unsigned()
				->comment('Активность')
            ;

            $table->char('uid', 80)
                ->unique()
				->comment('uid 1C')
            ;

            $table->index('uid');
            $table->index('active');
            $table->index('code');

        });        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
