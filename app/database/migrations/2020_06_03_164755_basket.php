<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Basket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('basket', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->timestamps();

            $table->bigInteger('order_id')
                ->unsigned()
				->comment('id заказав в лендинге')
            ;

            $table->integer('offer_id')
                ->unsigned()
				->comment('id торгового предложения')
            ;

            $table->integer('store_id')
                ->unsigned()
				->comment('id склада')
            ;

            $table->float('price', 12, 2)
				->comment('Цена торгового предложения в заказе')
            ;

            $table->integer('amount')
				->comment('Количество единиц')
            ;

            $table->index('order_id');
            $table->index('offer_id');
            $table->index('store_id');

            $table->foreign("offer_id")->references("id")->on("offer");
            $table->foreign("order_id")->references("id")->on("landing_order");
            $table->foreign("store_id")->references("id")->on("store");

        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
