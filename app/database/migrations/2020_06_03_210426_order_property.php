<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderProperty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('order_property', function (Blueprint $table) {

            $table->tinyIncrements('id');
            $table->timestamps();

            $table->char('name', 255)
                ->unique()
                ->comment('Название свойства')
            ;

            $table->char('code', 255)
                ->unique()
				->comment('Код свойства')
            ;

            $table->index('code');

        });        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
