<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProtectedMethod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('protected_method', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->char("name", 64)
                ->unique()
                ->comment('Название защищеного метода')
            ;
        });            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
