<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductBage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('product_bage', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->integer('product_id')
                ->unsigned()
				->comment('Товар чей бейдж')
            ;

            $table->integer('bage_id')
                ->unsigned()
				->comment('id бейджа')
            ;

            $table->index('product_id');
            $table->index('bage_id');
            $table->foreign("product_id")->references("id")->on("product");
            $table->foreign("bage_id")->references("id")->on("bage");
        });        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
