<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Offer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('offer', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->integer('product_id')
                ->unsigned()
                ->comment('id товара для которого торговое предложение')
            ;

            $table->char('uid', 80)
                ->unique()
				->comment('uid 1C')
            ;

            $table->char('name', 255)
                ->unique()
                ->comment('Название торгового предложения')
            ;

            $table->char('code', 255)
                ->unique()
				->comment('Код торгового предложения')
            ;

            $table->float('price', 12, 2)
				->comment('Цена торгового предложения')
            ;

            $table->tinyInteger('active')
                ->unsigned()
				->comment('Активность')
            ;

            $table->index('active');
            $table->index('uid');
            $table->index('code');

            $table->index('product_id');
            $table->foreign("product_id")->references("id")->on("product");
        });        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
