<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderPropertyValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('order_property_value', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->timestamps();

            $table->tinyInteger("order_property_id")
                ->unsigned()
                ->comment('id свойства заказа')
            ;

            $table->bigInteger("order_id")
                ->unsigned()
                ->comment('id заказа для которого свойство')
            ;

            $table->char("value", 32)
                ->unsigned()
                ->comment('id заказа для которого свойство')
            ;

            $table->index('order_property_id');
            $table->index('order_id');

            $table->foreign("order_property_id")->references("id")->on("order_property");
            $table->foreign("order_id")->references("id")->on("landing_order");


        });        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
