<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Property extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('property', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->char('name', 32)
				->comment('Имя свойства')
            ;

            $table->tinyInteger('sale')
                ->unsigned()
				->comment('Является ли свойство торговой характеристикой (например, цветом)')
            ;
            
            $table->char('code', 64)
                ->unique()
				->comment('Код свойства')
            ;

            $table->char('uid', 80)
                ->unique()
				->comment('uid 1C')
            ;

            $table->index('code');
            $table->index('sale');

        });        




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
