<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Product extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('product', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->tinyInteger('transport_id')
                ->unsigned()
                ->comment('ID транспорта, выполняющего заказы по товарам')
            ;

            $table->integer('section_id')
                ->unsigned()
                ->comment('ID раздела')
            ;

            $table->char('uid', 80)
                ->unique()
				->comment('uid 1C')
            ;

            $table->char('name', 255)
                ->unique()
                ->comment('Название товара')
            ;

            $table->char('code', 255)
                ->unique()
				->comment('Код товара')
            ;
            $table->tinyInteger('active')
                ->unsigned()
				->comment('Активность')
            ;

            $table->index('active');
            $table->index('transport_id');
            $table->index('section_id');
            $table->index('uid');
            $table->index('code');

            $table->foreign("transport_id")->references("id")->on("transport");
            $table->foreign("section_id")->references("id")->on("section");

        });        



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
