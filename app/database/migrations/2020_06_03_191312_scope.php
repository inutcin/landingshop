<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Scope extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('scope', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->integer("protected_method_id")
                ->unsigned()
                ->comment('id защищённого метода')
            ;

            $table->integer("token_id")
                ->unsigned()
                ->comment('id токена')
            ;

            $table->index("protected_method_id");
            $table->index("token_id");

            $table->foreign("protected_method_id")->references("id")->on("protected_method");
            $table->foreign("token_id")->references("id")->on("token");
            
        });            

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
