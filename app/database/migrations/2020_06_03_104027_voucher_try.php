<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VoucherTry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_try', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->timestamps();

            $table->bigInteger('user_id')
				->unique()
                ->unsigned()
				->comment('Пользователь')
            ;

            $table->char('name', 32)
				->comment('Имя')
            ;

            $table->tinyInteger('failed_tries')
                ->unsigned()
                ->comment('Колиичество неудачных попыток')
            ;

            $table->date('last_try_date')
				->comment('Дата последней попытки')
            ;

            $table->index('user_id');
            $table->foreign("user_id")->references("id")->on("user");
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
