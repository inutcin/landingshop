<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductProperty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('product_property', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->integer('product_id')
                ->unsigned()
				->comment('id товара чьё свойство')
            ;

            $table->integer('property_id')
                ->unsigned()
				->comment('id свойства товара')
            ;

            $table->integer('enum_id')
                ->unsigned()
				->comment('id значения свойства')
            ;
            
            $table->index('product_id');
            $table->index('property_id');
            $table->index('enum_id');

            $table->foreign("product_id")->references("id")->on("product");
            $table->foreign("property_id")->references("id")->on("property");
            $table->foreign("enum_id")->references("id")->on("property_enum");

        });        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
