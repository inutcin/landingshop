<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TransactionComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_comment', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->timestamps();

            $table->bigInteger('transaction_id')
				->unique()
                ->unsigned()
				->comment('id транзакции')
            ;

            $table->char('comment', 255)
				->comment('Комемнтарий к транзакции')
            ;
            
            $table->index('transaction_id');
            $table->foreign("transaction_id")->references("id")->on("transaction");
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
