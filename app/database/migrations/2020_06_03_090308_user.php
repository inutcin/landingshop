<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class User extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->timestamps();

            $table->char('ssoid', 48)
				->unique()
				->comment('SSOID в mos.ru')
            ;

            $table->bigInteger('ag_id')
				->unique()
                ->unsigned()
				->comment('АГ id')
            ;

            $table->float('brown_points_total',12,2)
				->comment('Количество баллов у пользователя')
            ;

            $table->date('login_at')
				->comment('Дата последней авторизации')
            ;

            $table->index('ssoid');
            $table->index('ag_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
