<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserBage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('user_bage', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->timestamps();

            $table->bigInteger('user_id')
                ->unsigned()
				->comment('Пользователь чей бейдж')
            ;

            $table->integer('bage_id')
                ->unsigned()
				->comment('id бейджа')
            ;

            $table->index('user_id');
            $table->index('bage_id');
            $table->foreign("user_id")->references("id")->on("user");
            $table->foreign("bage_id")->references("id")->on("bage");
        });        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
