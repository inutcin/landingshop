<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('product_file', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->integer('product_id')
                ->unsigned()
				->comment('Товар чей файл')
            ;

            $table->integer('file_id')
                ->unsigned()
				->comment('id файла')
            ;

            $table->index('product_id');
            $table->index('file_id');
            $table->foreign("product_id")->references("id")->on("product");
            $table->foreign("file_id")->references("id")->on("attached_file");
        });        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
