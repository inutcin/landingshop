<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PropertyEnum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('property_enum', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->integer('property_id')
                ->unsigned()
                ->comment('Свойство, чьё это значение')
            ;

            $table->char('uid', 80)
                ->unique()
				->comment('uid 1C')
            ;

            $table->char('value', 64)
				->comment('Значение')
            ;

            $table->index('property_id');
            $table->index('uid');

            $table->foreign("property_id")->references("id")->on("property");
        });        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
