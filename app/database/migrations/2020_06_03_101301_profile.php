<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Profile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->timestamps();

            $table->bigInteger('user_id')
				->unique()
                ->unsigned()
				->comment('Пользователь чей профиль')
            ;

            $table->char('name', 32)
				->comment('Имя')
            ;

            $table->char('last_name', 32)
				->comment('Фамилия')
            ;

            $table->char('second_name', 32)
				->comment('Отчество')
            ;

            $table->date('birthdate')
				->comment('Дата рождения')
            ;

            $table->char('phone', 20)
				->comment('Телефон')
            ;

            $table->char('email', 128)
				->comment('Email')
            ;

            $table->index('user_id');
            $table->foreign("user_id")->references("id")->on("user");
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
