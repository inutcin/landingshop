<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OfferProperty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('offer_property', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->integer('offer_id')
                ->unsigned()
				->comment('id торгового предложения чьё свойство')
            ;

            $table->integer('property_id')
                ->unsigned()
				->comment('id свойства торгового предложения')
            ;

            $table->integer('enum_id')
                ->unsigned()
				->comment('id значения свойства')
            ;
            
            $table->index('offer_id');
            $table->index('property_id');
            $table->index('enum_id');

            $table->foreign("offer_id")->references("id")->on("offer");
            $table->foreign("property_id")->references("id")->on("property");
            $table->foreign("enum_id")->references("id")->on("property_enum");

        });        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
