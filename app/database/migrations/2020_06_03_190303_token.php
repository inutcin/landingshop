<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Token extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('token', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->char("value", 40)
                ->unique()
                ->comment('текст токена')
            ;

            $table->date("expires")
                ->comment('Дата истечения токена')
            ;

            $table->tinyInteger("active")
                ->unsigned()
                ->comment('Активность токена')
            ;

        });           
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
