<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Available extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('available', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->timestamps();

            $table->integer('offer_id')
                ->unsigned()
				->comment('id торгового предложения')
            ;

            $table->integer('store_id')
                ->unsigned()
				->comment('id склада')
            ;

            $table->integer('amount')
				->comment('Количество единиц')
            ;

            $table->index('offer_id');
            $table->index('store_id');

            $table->foreign("offer_id")->references("id")->on("offer");
            $table->foreign("store_id")->references("id")->on("store");

        });          
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
