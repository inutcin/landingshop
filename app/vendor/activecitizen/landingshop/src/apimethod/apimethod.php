<?php
namespace activecitizen\landingshop\apimethod;


class apimethod extends \activecitizen\landingshop\common
{
    protected $data = [];
    private $t0;
    private $t1;

    public $httpCode = 200;
    
    function __construct(){
        $this->t0 = microtime(true);
    }
    
    function success(array $jsonParams = []){
        
        return [
            "errorCode"     =>  0, 
            "errorMessage"  =>  '',
            "execTime"      =>  round(microtime(true) - $this->t0,8), 
            "request_id"    =>  uniqid('', true),
            "result"=>$this->data
        ];
    }
}

