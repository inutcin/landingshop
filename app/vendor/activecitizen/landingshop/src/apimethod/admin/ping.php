<?php
namespace activecitizen\landingshop\apimethod\admin;
use activecitizen\landingshop\apimethod as apimethod;

class ping extends apimethod\apimethod
{
    function execute(array $jsonParams=[]){
        $this->data = null;
        return $this->success($jsonParams);
    }
}
