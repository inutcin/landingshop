<?php
namespace activecitizen\landingshop;

class api extends common
{

    /**
        Получаем объект необходимого API-метода
    */
    function getInstance(string $methodName){
        $methodName = str_replace("/","\\", $methodName);
        $className = "activecitizen\\landingshop\\apimethod\\".$methodName;
        if(!class_exists($className))
            return $this->addError("Api method ".htmlspecialchars($methodName)." is not exists.");
        return new $className();
    }

}
