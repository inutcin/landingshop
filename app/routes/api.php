<?php

use Illuminate\Http\Request;
use activecitizen\landingshop as landingshop;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::get('{methodName}',function($methodName, Request $request){
    // Имя класса обработчика метода API
    $api = new landingshop\api;

    // Получаем объект API-метода
    $method = $api->getInstance($methodName);
    
    // Обрабатываем ошибки инициализации объекта
    if($errors = $api->getErrors())
        return response()
            ->json(["errors_count"=>count($errors), "errors"=>$errors], 500)
            ->header("Access-Control-Allow-Origin", "*");
        
    // Получаем ответ метода
    $postData = $request->getContent();
    $postRequest = json_decode($postData,true);
    if(!is_array($postRequest))$postRequest = [];
    $methodResult = $method->execute($postRequest);

    return response()
        ->json($methodResult,$method->httpCode)
        ->header("Access-Control-Allow-Origin", "*");
})->where(["methodName"=>'[a-zA-Z][a-zA-Z0-9]+/[a-zA-Z][a-zA-Z0-9]+']);


Route::match(['head','put','option'],'{methodName}',function($methodName, Request $request){
    return response()
        ->json('Not supported', 405)
        ->header("Access-Control-Allow-Origin", "*");
})->where(["methodName"=>'.*']);
