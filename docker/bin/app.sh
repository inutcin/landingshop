#!/bin/bash

if ! [ -d /var/www/html/vendor/laravel ]; then
    cd /var/www/html
    composer install
#    sudo -u www-data composer create-project --prefer-dist laravel/laravel laravel
    cp /var/www/html/.env.sample /var/www/html/.env
    php /var/www/html/artisan key:generate
fi

connectstring="host=$DB_HOST port=$DB_PORT dbname=$DB_DATABASE user=$DB_USERNAME password=$DB_PASSWORD"
echo $connectstring;

while php -r "if(pg_connect('$connectstring')){echo 'Database server is ready. Connected.';exit(1);}else{exit(0);}"; do
  >&2 echo "Database server is unavailable - sleeping"
  sleep 1
done

chown -R www-data /var/www/html

php /var/www/html/artisan migrate --force

php-fpm -F -c /etc/php/7.2/fpm/php.ini -y /etc/php/7.2/fpm/php-fpm.conf


